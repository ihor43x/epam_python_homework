"""01-Task1-Task1
Write a Python-script that performs simple arithmetic operations: '+', '-', '*', '/'. The type of operator and
data are set on the command line when the script is run.
The script should be launched like this:
$ python my_task.py 1 * 2

Notes:
For other operations need to raise NotImplementedError.
Do not dynamically execute your code (for example, using exec()).
Use the argparse module to parse command line arguments. Your implementation shouldn't require entering any
parameters (like -f or --function).
"""
import argparse


def calculate(a, action, b):
    operations = {"+": lambda x, y: x + y,
                  "-": lambda x, y: x - y,
                  "*": lambda x, y: x * y,
                  "/": lambda x, y: x / y}
    if action in operations:
        return operations.get(action)(a, b)
    else:
        raise NotImplementedError


def main():
    parser = argparse.ArgumentParser(description='Task 1, ex. 1')
    parser.add_argument('a', type=float)
    parser.add_argument('action')
    parser.add_argument('b', type=float)
    args = parser.parse_args()
    result = calculate(args.a, args.action, args.b)
    print(float(result))


if __name__ == '__main__':
    main()
