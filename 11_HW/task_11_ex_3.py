"""
Task 3

Implement a decorator `call_once` which runs `sum_of_numbers` function once and caches the result.
All consecutive counter to this function should return cached result no matter the arguments.

Example:
@call_once
def sum_of_numbers(a, b):
    return a + b

print(sum_of_numbers(13, 42))

# >>> 55

print(sum_of_numbers(999, 100))

# >>> 55

print(sum_of_numbers(134, 412))

# >>> 55
"""


def call_once(func):
    first_call = ""

    def wrapper(*args):
        nonlocal first_call
        if not first_call:
            first_call = func(*args)
        return first_call

    return wrapper


@call_once
def sum_of_numbers(a, b):
    return a + b


print(sum_of_numbers(13, 42))
print(sum_of_numbers(999, 100))
print(sum_of_numbers(134, 412))
