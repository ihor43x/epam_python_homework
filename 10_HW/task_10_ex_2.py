"""Implement a function `most_common_words(file_path: str, top_words: int) -> list`
which returns most common words in the file.
<file_path> - system path to the text file
<top_words> - number of most common words to be returned

Example:

print(most_common_words(file_path, 3))
'>>> ['donec', 'etiam', 'aliquam']
> NOTE: Remember about dots, commas, capital letters etc.
"""
from collections import Counter
import re


def most_common_words(text, top_words):
    with open(text) as f:
        words = re.findall(r'''\w+''', f.read())
    c = Counter(words)
    result = list(map(lambda x: x[0], c.most_common(top_words)))
    return result


if __name__ == '__main__':
    file_path = "data/task_10.2_test.txt"
    print(most_common_words(file_path, 10))
