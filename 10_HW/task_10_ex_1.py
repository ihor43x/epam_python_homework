"""
Implement a function `sort_names(input_file_path: str, output_file_path: str) -> None`, which sorts names from
`file_path` and write them to a new file `output_file_path`. Each name should start with a new line as in the
following example:
Example:

Adele
Adrienne
...
Willodean
Xavier
"""


def sort_names(input_file_path: str, output_file_path: str) -> None:
    with open(input_file_path) as input_file:
        names = [name.rstrip("\n") for name in input_file]
    with open(output_file_path, "w") as output_file:
        for name in sorted(names):
            output_file.write(name + "\n")


if __name__ == '__main__':
    original_file_path = "data/task_10.1_test.txt"
    sorted_file_path = f"{original_file_path.replace('.txt','')}_sorted.txt"
    sort_names(original_file_path, sorted_file_path)
    with open(sorted_file_path) as f:
        print(f.read(), end="")
