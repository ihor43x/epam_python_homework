"""
File `data/students.csv` stores information about students in CSV format.
This file contains the student’s names, age and average mark.

1. Implement a function get_top_performers which receives file path and
returns names of top performer students.
Example:
def get_top_performers(file_path, number_of_top_students=5):
    pass

print(get_top_performers("students.csv"))

Result:
['Teresa Jones', 'Richard Snider', 'Jessica Dubose', 'Heather Garcia',
'Joseph Head']

2. Implement a function write_students_age_desc which receives the file path
with students info and writes CSV student information to the new file in
descending order of age.
Example:
def write_students_age_desc(file_path, output_file):
    pass

Content of the resulting file:
student name,age,average mark
Verdell Crawford,30,8.86
Brenda Silva,30,7.53
...
Lindsey Cummings,18,6.88
Raymond Soileau,18,7.27
"""
import csv


def get_top_performers(file_path, number_of_top_students=5):
    """
    Sorts students by descending by "average mark" column and returns a list
    of student names, who has the highest marks.
    :param file_path: path to CSV file.
    :param number_of_top_students: required number of the best students
    :return: list of student names
    """
    with open(file_path, newline='') as f:
        reader = csv.DictReader(f)
        students = [row for row in reader]

    students.sort(key=lambda x: float(x['average mark']), reverse=True)
    result = [student['student name'] for student in students[:number_of_top_students]]
    return result


def write_students_age_desc(file_path, output_file):
    """
    Sorts students from the input CSV file by age in ascending order
    and creates new CSV file with the sorted list.
    :param file_path: path to input CSV file
    :param output_file: path to sorted CSV file
    :return: none
    """
    with open(file_path, newline='') as f1:
        reader = csv.DictReader(f1)
        students = [row for row in reader]
        fieldnames = reader.fieldnames
    students.sort(key=lambda x: x['age'], reverse=True)
    with open(output_file, 'w', newline='') as f2:
        writer = csv.DictWriter(f2, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(students)


if __name__ == '__main__':
    input_file = "data/students.csv"
    output_file = "data/students_sorted.csv"
    print(get_top_performers(input_file, 5))
    print(write_students_age_desc(input_file, output_file))
