"""
Develop Rectangle class with following content:
    2 private fields type of float `side_a` and `side_b` (sides А and В of the rectangle);
    One constructor with two optional parameters a and b (parameters specify rectangle sides). Side А of a rectangle
    defaults to 4, side В - 3. Raise ValueError if received parameters are less than or equal to 0;
    Method `get_side_a`, returning value of the side А;
    Method `get_side_b`, returning value of the side В;
    Method `area`, calculating and returning the area value;
    Method `perimeter`, calculating and returning the perimeter value;
    Method `is_square`, checking whether current rectangle is square or not. Returns True if the shape is square and
    False in another case;
    Method `replace_sides`, swapping rectangle sides.

Develop class ArrayRectangles, in which declare:
    Private attribute `rectangle_array` (list of rectangles);
    One constructor that creates a list of rectangles with length `n` filled with `None` and that receives an
    arbitrary amount of objects of type `Rectangle` or a list of objects of type `Rectangle` (the list must be
    unpacked inside the constructor so that there will be no nested arrays). If both objects and length are passed,
    at first creates a list with received objects and then add the required number of Nones to achieve the
    desired length. If `n` is less than the number of received objects, the length of the list will be equal to the
    number of objects;
    Method `add_rectangle` that adds a rectangle of type `Rectangle` to the array on the nearest free place and
    returning True, or returning False, if there is no free space in the array;
    Method `number_max_area`, that returns order number (index) of the first rectangle with the maximum area value
    (numeration starts from zero);
    Method `number_min_perimeter`, that returns order number (index) of the first rectangle with the minimum area value
    (numeration starts from zero);
    Method `number_square`, that returns the number of squares in the array of rectangles
"""


class Rectangle:

    def __init__(self, a=4, b=3):
        self.check_parameters(a, b)
        self.__side_a = a
        self.__side_b = b

    @staticmethod
    def check_parameters(a, b):
        if a <= 0 or b <= 0:
            raise ValueError

    def get_side_a(self):
        return self.__side_a

    def get_side_b(self):
        return self.__side_b

    def area(self):
        return self.__side_a * self.__side_b

    def perimeter(self):
        return (self.__side_a + self.__side_b) * 2

    def is_square(self):
        return self.__side_a == self.__side_b

    def replace_sides(self):
        self.__side_a, self.__side_b = self.__side_b, self.__side_a


class ArrayRectangles:

    def __init__(self, *args, n=0):
        self.__n = n
        self.__rectangle_array = []
        for arg in args:
            if isinstance(arg, Rectangle):
                self.__rectangle_array.append(arg)
            elif isinstance(arg, list):
                self.__rectangle_array.extend(arg)
        if n > len(args):
            for i in range(n - len(args)):
                self.__rectangle_array.append(None)

    def add_rectangle(self, new_rectangle: Rectangle):
        if None in self.__rectangle_array:
            none_index = self.__rectangle_array.index(None)
            self.__rectangle_array[none_index] = new_rectangle
            return True
        else:
            return False

    def number_max_area(self):
        max_area_rect = max((((rect.area(), rect) for rect in self.__rectangle_array if rect is not None)),
                            key=lambda x: x[0])
        return self.__rectangle_array.index(max_area_rect[1])

    def number_min_perimeter(self):
        min_perimeter_rect = min((((rect.perimeter(), rect) for rect in self.__rectangle_array if rect is not None)),
                                 key=lambda x: x[0])
        return self.__rectangle_array.index(min_perimeter_rect[1])

    def number_square(self):
        return sum((1 for rect in self.__rectangle_array if rect is not None and rect.is_square()))


if __name__ == '__main__':
    rect_1 = Rectangle(20, 20)
    rect_2 = Rectangle(1, 1)
    rect_3 = Rectangle(2, 2)

    # print(rect_1.perimeter() / rect_1.area())
    # print(rect_2.perimeter() / rect_2.area())

    rect_arr = ArrayRectangles([rect_1, rect_2], n=6)
    rect_arr_2 = ArrayRectangles(rect_1, rect_2, n=6)

    # rect_arr.add_rectangle(rect_2)
    # rect_arr.add_rectangle(rect_3)
    # print(rect_arr.number_max_area())
    # print(rect_arr.number_min_perimeter())
    # rect_arr_2 = ArrayRectangles(rect_1, rect_2)
    print(rect_arr_2.number_square())
    # print(rect_arr_2)
    # print(f"a={rect_1.get_side_a()}, b={rect_1.get_side_b()}")
    # print(f"area: {rect_1.area()}")
    # print(f"perimeter: {rect_1.perimeter()}")
    # rect_1.replace_sides()
    # print(f"area: {rect_1.area()}")
    # print(f"perimeter: {rect_1.perimeter()}")
    # print(f"is square: {rect_1.is_square()}")
    # print(f"a={rect_1.get_side_a()}, b={rect_1.get_side_b()}")
