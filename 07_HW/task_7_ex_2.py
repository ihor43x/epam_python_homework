"""
Task 7_2
Create classes Employee, SalesPerson, Manager and Company with predefined functionality.

Create basic class Employee and declare following content:
• Attributes – `name` (str), `salary` and `bonus` (int), set with property decorator
• Constructor - parameters `name` and `salary`
• Method `bonus` - sets bonuses to salary, amount of which is delegated as `bonus`
• Method `to_pay` - returns the value of summarized salary and bonus.

Create class SalesPerson as class Employee inheritor and declare within it:
• Constructor with parameters: `name`, `salary`, `percent` – percent of plan performance (int, without the % symbol),
first two of which are passed from basic class constructor
• Redefine method of parent class `bonus` in the following way: if the sales person completed the plan more than 100%,
their bonus is doubled (is multiplied by 2), and if more than 200% - bonus is tripled (is multiplied by 3)

Create class Manager as Employee class inheritor, and declare within it:
• Constructor with parameters: `name`, `salary` and `client_number` (int, number of served clients), first two of which
are passed to basic class constructor.
• Redefine method of parent class `bonus` in the following way: if the manager served over 100 clients, their bonus
is increased by 500, and if more than 150 clients – by 1000.

Create class Company and declare within it:
• Constructor with parameters: `employees` – list of company`s employees (made up of Employee/SalesPerson/Manager
classes instances) with arbitrary length `n`
• Method `give_everybody_bonus` with parameter company_bonus (int) that sets
the amount of basic bonus for each employee.
• Method `total_to_pay` that returns total amount of salary of all employees including awarded bonus
• Method `name_max_salary` that returns name of the employee, who received maximum salary including bonus.

Note:
Class attributes and methods should bear exactly the same names as those given in task description
Methods should return only target values, without detailed explanation within `return`
"""


class Employee:

    def __init__(self, name: str, salary: int, bonus=0):
        self.__name = name
        self.__salary = salary
        self.__bonus = bonus

    @property
    def bonus(self):
        return self.__bonus

    @bonus.setter
    def bonus(self, value):
        self.__bonus = value

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.__name = value

    @property
    def salary(self):
        return self.__salary

    @salary.setter
    def salary(self, value):
        self.__salary = value

    def to_pay(self):
        return self.__salary + self.__bonus


class SalesPerson(Employee):

    def __init__(self, name: str, salary: int, percent: int):
        super().__init__(name, salary)
        self.__percent = percent

    @property
    def bonus(self):
        return self.__bonus

    @bonus.setter
    def bonus(self, new_bonus):
        if self.__percent >= 200:
            self.__bonus = new_bonus * 3
        elif self.__percent >= 100:
            self.__bonus = new_bonus * 2
        else:
            self.__bonus = new_bonus


class Manager(Employee):

    def __init__(self, name: str, salary: int, client_number: int):
        super().__init__(name, salary)
        self.__client_number = client_number

    @property
    def bonus(self):
        return self.__bonus

    @bonus.setter
    def bonus(self, new_bonus):
        if self.__client_number >= 150:
            self.__bonus = new_bonus + 1000
        elif self.__client_number >= 100:
            self.__bonus = new_bonus + 500
        else:
            self.__bonus = new_bonus


class Company:

    def __init__(self, employees, **kwargs):
        self.__employees = employees

    @property
    def employees(self):
        return self.__employees

    @employees.setter
    def employees(self, value):
        self.__employees.append(value)

    def give_everybody_bonus(self, company_bonus: int):
        for employee in self.__employees:
            employee.__bonus = company_bonus

    def total_to_pay(self):
        return sum(((employee.to_pay() for employee in self.__employees)))

    def name_max_salary(self):
        max_salary = max((((employee.to_pay(), employee.name) for employee in self.__employees)), key=lambda x: x[0])
        return max_salary[1]


if __name__ == '__main__':
    emp = Employee("Ihor", 6000)
    emp.bonus = 1500
    print(emp.name, emp.bonus, emp.to_pay())

    sls = SalesPerson("Vasyl", 4000, 250)
    sls.bonus = 1100
    print(sls.name, sls.bonus, sls.to_pay())

    mngr = Manager("Nazar", 5000, 120)
    mngr.bonus = 600
    print(mngr.name, mngr.bonus, mngr.to_pay())

    company = Company([Employee('Peter', 87_000), Manager('Gigi', 10_000, 267), SalesPerson('Andy', 90_000, 180)], n=3)
    print(company.total_to_pay())
    print(company.name_max_salary())
    company.employees = Employee('Peter', 87_000)
