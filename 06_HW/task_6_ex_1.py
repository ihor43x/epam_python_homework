"""
Implement function combine_dicts, which receives a changeable
number of dictionaries (keys - letters, values - integers)
and combines them into one dictionary.

Dict values should be summarized in case of identical keys.

Example:
dict_1 = {'a': 100, 'b': 200}
dict_2 = {'a': 200, 'c': 300}
dict_3 = {'a': 300, 'd': 100}

combine_dicts(dict_1, dict_2)
Result: {'a': 300, 'b': 200, 'c': 300}

combine_dicts(dict_1, dict_2, dict_3)
Result: {'a': 600, 'b': 200, 'c': 300, 'd': 100}
"""


def check_key_val_errors(key, val):
    if not key.isalpha() or len(key) > 1:
        raise KeyError
    if type(val) is not int:
        raise ValueError


def combine_dicts(*dicts):
    output_dict = {}
    for one_dict in dicts:
        for key, val in one_dict.items():
            check_key_val_errors(key, val)
            if output_dict.get(key):
                output_dict[key] += val
            else:
                output_dict[key] = val
    return output_dict


if __name__ == '__main__':
    dict_1 = {'a': 100, 'b': 200}
    dict_2 = {'a': 200, 'c': 300}
    dict_3 = {'a': 300, 'd': 100}
    print(f'Result: {combine_dicts(dict_1, dict_2)}')
    print(f'Result: {combine_dicts(dict_1, dict_2, dict_3)}')
