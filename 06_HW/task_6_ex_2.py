"""
Create CustomList – the linked list of values of random type, which size changes dynamically and has an ability to index
elements.

The task requires implementation of the following functionality:
• Create the empty user list and the one based on enumeration of values separated by commas. The elements are stored
in form of unidirectional linked list. Nodes of this list must be implemented in class Item.
    Method name: __init__(self, *data) -> None;
• Add and remove elements.
    Method names: append(self, value) -> None - to add to the end,
                add_start(self, value) -> None - to add to the start,
                remove(self, value) -> None - to remove the first occurrence of given value;
• Operations with elements by index. Negative indexing is not necessary.
    Method names: __getitem__(self, index) -> Any,
                __setitem__(self, index, data) -> None,
                __delitem__(self, index) -> None;
• Receive index of predetermined value.
    Method name: find(self, value) -> Any;
• Clear the list.
    Method name: clear(self) -> None;
• Receive lists length.
    Method name: __len__(self) -> int;
• Make CustomList iterable to use in for-loops;
    Method name: __iter__(self);
• Raise exceptions when:
    find() or remove() don't find specific value
    index out of bound at methods __getitem__, __setitem__, __delitem__.


Notes:
    The class CustomList must not inherit from anything (except from the object, of course).
    Function names should be as described above. Additional functionality has no naming requirements.
    Indexation starts with 0.
    List length changes while adding and removing elements.
    Inside the list the elements are connected as in a linked list, starting with link to head.
"""


class Item:
    """
    A node in a unidirectional linked list.
    """

    def __init__(self, value, next_item=None):
        self.value = value
        self.next = next_item


class CustomList:
    """
    An unidirectional linked list.
    """

    def __init__(self, *data):
        self.head = None
        for value in data:
            self.append(value)

    def __repr__(self):
        result = []
        head = self.head
        while head is not None:
            result.append(str(head.value))
            head = head.next
        return ", ".join(result)

    def __len__(self):
        head = self.head
        i = 0
        while head is not None:
            head = head.next
            i += 1
        return i

    def __getitem__(self, index):
        head = self.head
        i = 0
        if i == index:
            return head.value
        while head is not None and i <= index:
            if i == index:
                return head.value
            i += 1
            head = head.next
        else:
            raise IndexError

    def __setitem__(self, index, data):
        head = self.head
        i = 0
        if i == index:
            head.value = data
        while head is not None and i <= index:
            if i == index:
                head.value = data
                return
            i += 1
            head = head.next
        else:
            raise IndexError

    def __delitem__(self, index):
        head = self.head
        i = 0
        if i == index:
            self.head = head.next
            return
        while head.next is not None and i <= index:
            i += 1
            if i == index:
                head.next = head.next.next
                return
            head = head.next
        else:
            raise IndexError

    def __iter__(self):
        self.iter = self.head
        return self

    def __next__(self):
        if self.iter is None:
            raise StopIteration
        else:
            value = self.iter.value
            self.iter = self.iter.next
            return value

    def append(self, value):
        new_item = Item(value)
        if self.head is None:
            self.head = new_item
            return
        else:
            head = self.head
            while head.next is not None:
                head = head.next
            else:
                head.next = new_item

    def add_start(self, value):
        new_item = Item(value)
        new_item.next = self.head
        self.head = new_item

    def find(self, value):
        head = self.head
        i = 0
        while head is not None:
            if head.value == value:
                return i
            head = head.next
            i += 1
        else:
            raise ValueError

    def remove(self, value):
        head = self.head
        if head.value == value:
            self.head = head.next
            return
        while head.next is not None:
            if head.next.value == value:
                head.next = head.next.next
                return
            head = head.next
        else:
            raise ValueError

    def clear(self):
        while self.head is not None:
            self.head = self.head.next

    def is_empty(self):
        return False if self.head else True


if __name__ == '__main__':
    test = CustomList(3, 4, 5)
    for j in range(99):
        test.append(j)
    print(test)
    # print(len(test))
    # test.remove(3)
    # print(test[2])
    # test[1] = 111
    # del test[2]
    # print(test.find(3))
    # test.clear()
    # test.append("I")
    # test.append(5)
    # print(test)
    # for item in test:
    #     print(item)
