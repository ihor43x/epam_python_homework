"""
try:
    if  '1'  !=  1:
        raise  "Error"
    else:
        print("Error  has  not  occurred")
except  "Error":
    print  ("Error  has  occurred")
"""


def foo():
    try:
        if '1' != 1:
            raise Exception("Error")
        else:
            print("Error has not occurred")
    except Exception:
        print("Error  has  occurred")


if __name__ == '__main__':
    foo()
