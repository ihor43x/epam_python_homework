"""
def  foo():
    try:
        print(1)
        return
    finally:
        print (2)
    else:
        print(3)
foo()
"""


def foo():
    try:
        print(1)
    except:
        print(2)
    else:
        print(3)
    finally:
        print(4)


if __name__ == '__main__':
    foo()
