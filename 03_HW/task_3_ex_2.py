"""Task 3_2
Write a function converting a Roman numeral from a given string N into an Arabic numeral.
Values may range from 1 to 100 and may contain invalid symbols.
Invalid symbols and numerals out of range should raise ValueError.

Numeral / Value:
I: 1
V: 5
X: 10
L: 50
C: 100

Example:
N = 'I'; result = 1
N = 'XIV'; result = 14
N = 'LXIV'; result = 64

Example of how the task should be called:
python3 task_3_ex_2.py LXIV

Note: use `argparse` module to parse passed CLI arguments
"""
import argparse
import re
import romnum

ROMAN_DICT = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}


def index_exist(sequence, ind):
    try:
        _ = sequence[ind]
        return True
    except IndexError:
        return False


def validate(args):
    regex = r'''[^IVXLCDM]|M{4,}|D{2,}|C{4,}|L{2,}|
    X{4,}|V{2,}|I{4,}|I[^I]I|V[^V]V|X[^X]X{2,}|
    L[^L]L|C[^C]C{2,}|D[^D]D|M[^M]M{2,}'''
    if re.search(regex, args):
        raise ValueError("Invalid Roman number!")


def from_roman_numerals(args):
    validate(args)
    numbers = [ROMAN_DICT.get(k) for k in args]
    for i, num in enumerate(numbers):
        # if the next number is bigger then current number must be negative
        if index_exist(numbers, i + 1):
            if num < numbers[i + 1]:
                numbers[i] *= -1
    return sum(numbers)


def main():
    parser = argparse.ArgumentParser(description="Task 3 ex. 2")
    parser.add_argument("roman_number", help="Roman number")
    args = parser.parse_args()
    print(from_roman_numerals(args.roman_number))


if __name__ == '__main__':
    main()

    # for testing comment line 51  and uncomment lines 53-55:
    # for number in romnum.romnum:
    #     result = from_roman_numerals(number)
    #     print(f"N = {number}; result = {result}")
