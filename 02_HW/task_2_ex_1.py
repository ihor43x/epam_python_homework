"""
Task 2_3

You are given n bars of gold with weights: w1, w2, ..., wn and bag with capacity W.
There is only one instance of each bar and for each bar you can either take it or not
(hence you cannot take a fraction of a bar). Write a function that returns the maximum weight of gold that fits
into a knapsack's capacity.

The first parameter contains 'capacity' - integer describing the capacity of a knapsack
The next parameter contains 'weights' - list of weights of each gold bar
The last parameter contains 'bars_number' - integer describing the number of gold bars
Output : Maximum weight of gold that fits into a knapsack with capacity of W.

Note:
Use the argparse module to parse command line arguments. You don't need to enter names of parameters (i. e. -capacity)
Raise ValueError in case of false parameter inputs
Example of how the task should be called:
python3 task3_1.py -W 56 -w 3 4 5 6 -n 4
"""
import argparse
import time


def bounded_knapsack(args):
    # Oleh's algorithm:
    capacity = args.W
    weights = args.w

    m = [0] * (capacity + 1)
    m[0] = 1

    for w in weights:
        for i in range(capacity, w - 1, -1):
            if m[i - w] == 1:
                m[i] = 1

    i = len(m) - 1
    while m[i] == 0:
        i -= 1
    return i

    # My algorithm:
    # try:
    #     capacity = args.W
    #     weights = sorted(args.w, reverse=True)
    #     bars_number = args.n
    #
    # except TypeError:
    #     raise ValueError
    # if (len(weights) != bars_number or
    #         capacity <= 0 or
    #         [n for n in weights if n <= 0] or
    #         capacity <= weights[-1] or
    #         has_duplicates(weights)):
    #     raise ValueError
    # else:
    #     selection = 0
    #     # selection = [w for w in weights]
    #     for w in weights:
    #         selection += w
    #         if selection == capacity:
    #             return selection
    #         elif selection > capacity:
    #             selection -= w
    #     return selection


def has_duplicates(list_original):
    list_copy = list_original.copy()
    for _ in list_original:
        if list_copy.pop() in list_copy:
            return True
    return False


def main():
    parser = argparse.ArgumentParser(description="Task 2 ex. 1")
    parser.add_argument("-W", type=int, help="capacity")
    parser.add_argument("-w", nargs="*", type=int, help="weights")
    parser.add_argument("-n", type=int, help="bars_number")
    args = parser.parse_args()
    # args = parser.parse_args("-W 200 -w 29 26 3 4 51 56 7 70 9 10 11 12\
    # 13 14 15 16 17 18 19 20 21 22 23 24 25 -n 25".split())
    # args = parser.parse_args("-W 20 -w 10 8 6 5 -n 4".split())
    t0 = time.time()
    print(bounded_knapsack(args))
    t1 = time.time()
    print(t1 - t0)


if __name__ == '__main__':
    main()
