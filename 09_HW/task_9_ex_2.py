"""
Write a function that checks whether a string is a palindrome or not.
Return 'True' if it is a palindrome, else 'False'.

Note:
Usage of reversing functions is required.
Raise ValueError in case of wrong data type

To check your implementation you can use strings from here
(https://en.wikipedia.org/wiki/Palindrome#Famous_palindromes).
"""
whitespace = ' \t\n\r\v\f'
punctuation = r"""!"#$%&'()*+,-–./:;<=>?@[\]^_`{|}~"""


def is_palindrome(test_string: str) -> bool:
    if isinstance(test_string, str):
        for char in whitespace + punctuation:
            test_string = test_string.replace(char, "")
        test_string = test_string.casefold()
        return test_string == "".join(reversed(test_string))
    else:
        raise ValueError


if __name__ == '__main__':
    print(is_palindrome("Able, was. I/ (ere I_saw Elba"))
    print(is_palindrome("Madam, I'm Adam"))
    print(is_palindrome("A man, a plan, a canal – Panama"))
    print(is_palindrome("True"))
    print(is_palindrome(123))
