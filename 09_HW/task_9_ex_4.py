"""
Implement a bunch of functions which receive a changeable number of strings and return next
parameters:
1) characters that appear in all strings
2) characters that appear in at least one string
3) characters that appear at least in two strings
  Note: raise ValueError if there are less than two strings
4) characters of alphabet, that were not used in any string
  Note: use `string.ascii_lowercase` for list of alphabet letters

Note: raise TypeError in case of wrong data type

Examples,
```python
test_strings = ["hello", "world", "python", ]
print(chars_in_all(*test_strings))
# >>> {'o'}
print(chars_in_one(*test_strings))
# >>> {'d', 'e', 'h', 'l', 'n', 'o', 'p', 'r', 't', 'w', 'y'}
print(chars_in_two(*test_strings))
# >>> {'h', 'l', 'o'}
print(not_used_chars(*test_strings))
# >>> {'q', 'k', 'g', 'f', 'j', 'u', 'a', 'c', 'x', 'm', 'v', 's', 'b', 'z', 'i'}
"""
import string


def chars_in_all(*strings):
    check_str_type(*strings)
    sets_of_chars = [set(s) for s in strings]
    result = sets_of_chars[0].intersection(*sets_of_chars[1:])
    return result


def chars_in_one(*strings):
    check_str_type(*strings)
    sets_of_chars = [set(s) for s in strings]
    result = sets_of_chars[0].union(*sets_of_chars[1:])
    return result


def chars_in_two(*strings):
    check_str_type(*strings)
    if len(strings) >= 2:
        sets_of_chars = [set(s) for s in strings]
        result = set()
        while len(sets_of_chars) > 1:
            current_set = sets_of_chars.pop()
            for char_set in sets_of_chars:
                result.update(current_set.intersection(char_set))
        return result
    else:
        raise ValueError


def not_used_chars(*strings):
    check_str_type(*strings)
    sets_of_chars = [set(s) for s in strings]
    all_chars = set(string.ascii_lowercase)
    result = all_chars.difference(*sets_of_chars)
    return result


def check_str_type(*args):
    for arg in args:
        if not isinstance(arg, str):
            raise TypeError


if __name__ == '__main__':
    # test_strings = ["hello"]
    # test_strings = [2, 3]
    test_strings = ["hello", "world", "python", ]
    print(chars_in_all(*test_strings))
    print(chars_in_one(*test_strings))
    print(chars_in_two(*test_strings))
    print(not_used_chars(*test_strings))
