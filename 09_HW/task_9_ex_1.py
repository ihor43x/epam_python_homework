"""
Task_9_1
Implement `swap_quotes` function which receives a string and replaces all " symbols with ' and vise versa.
The function should return modified string.

Note:
Usage of built-in or string replacing functions is required.
"""


def swap_quotes(string: str) -> str:
    return string.translate(string.maketrans({'"': "'", "'": '"'}))

    # Alternative:
    # string = string.replace("'", "<sq_sign>").replace('"', "<dq_sign>")
    # string = string.replace("<sq_sign>", '"').replace("<dq_sign>", "'")
    # return string

if __name__ == '__main__':
    print(swap_quotes("some'st\"ring"))
    print(swap_quotes('!?..."""""-%&^"@_"'))
    print(swap_quotes('\' " \'\' ""'))
    print(swap_quotes('anoth\'er"string'))