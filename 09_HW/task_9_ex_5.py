"""
Implement function count_letters, which takes string as an argument and
returns a dictionary that contains letters of given string as keys and a
number of their occurrence as values.

Example:
print(count_letters("Hello world!"))
Result: {'H': 1, 'e': 1, 'l': 3, 'o': 2, 'w': 1, 'r': 1, 'd': 1}

Note: Pay attention to punctuation.
"""
from collections import Counter
import string


def count_letters(text):
    if isinstance(text, str):
        result = Counter([char for char in text if char in string.ascii_letters])
        return dict(result)
    else:
        raise TypeError


if __name__ == '__main__':
    print(count_letters("Hello world!"))
