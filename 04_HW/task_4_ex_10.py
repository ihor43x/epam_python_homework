"""
Exercise 10
Create a function sum_binary_1 that for a positive
integer n calculates the result value,
which is equal to the sum of the “1” in the binary representation of n
otherwise, returns None.
Example,
n = 14 = 1110 result = 3
n = 128 = 10000000 result = 1
"""


def sum_binary_1(number):
    if isinstance(number, int) and number > 0 and not isinstance(number, bool):
        result = sum([int(n) for n in str(bin(number)) if n.isdecimal()])
        return result


def main():
    print(sum_binary_1(14))
    print(sum_binary_1(128))
    # print(sum_binary_1(-128))
    # print(sum_binary_1(0))
    # print(sum_binary_1(True))
    # print(sum_binary_1(False))
    # print(sum_binary_1("True"))
    # print(sum_binary_1([1, 2]))
    # print(sum_binary_1(1.3))
    # print(sum_binary_1((1, 2)))


if __name__ == '__main__':
    main()
