"""
Exercise 8
Implement a function which takes a list of elements and returns
a list of tuples containing pairs of this elements.
Pairs should be formed as in the example.
If there is only one element in the list return None instead.
Using zip() is prohibited.
Examples:
# >>> get_pairs([1, 2, 3, 8, 9])
# [(1, 2), (2, 3), (3, 8), (8, 9)]
# >>> get_pairs(['need', 'to', 'sleep', 'more’])
# [('need', 'to'), ('to', 'sleep'), ('sleep', 'more’)]
# >>> get_pairs([1])
None
"""


def get_pairs(elements):
    result = [(elem, elements[i + 1]) for i, elem in enumerate(elements) if (i < len(elements) - 1)]
    return result or None

    # Alternative:
    # result = []
    # for i, elem in enumerate(elements):
    #     if i < len(elements) - 1:
    #         result.append((elem, elements[i + 1]))
    # return result or None


def main():
    print(get_pairs([1, 2, 3, 8, 9]))
    print(get_pairs(['need', 'to', 'sleep', 'more']))
    print(get_pairs([1]))


if __name__ == '__main__':
    main()
