"""
Exercise 9
For a positive integer n calculate the result value,
which is equal to the sum of the odd numbers of n.
Example,
n = 1234 result = 4
n = 246 result = 0
Write it as function.
Note:
- Raise TypeError in case of wrong data type or negative integer;
- Use of 'functools' module is prohibited, you just need simple for loop.
"""


def sum_of_odds(number):
    if not isinstance(number, int) or isinstance(number, bool) or number <= 0:
        raise TypeError
    result = sum([int(n) for n in str(number) if int(n) % 2 == 1])
    return result


def main():
    print(sum_of_odds(1234))
    print(sum_of_odds(246))
    # print(sum_of_odds(True))
    # print(sum_of_odds(-10))


if __name__ == '__main__':
    main()
