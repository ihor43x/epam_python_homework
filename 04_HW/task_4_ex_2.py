"""
Exercise 2
Write is_palindrome function that checks whether a string is a palindrome or not.
Returns 'True' if it is palindrome, else 'False’.
To check your implementation you can use strings from here
(https://en.wikipedia.org/wiki/Palindrome#Famous_palindromes).
Note:
- Usage of any reversing functions is prohibited
- The function has to ignore special characters, whitespaces and different cases
- Raise ValueError in case of wrong data type

"""


def is_palindrome(phrase):
    if type(phrase) != str:
        raise ValueError
    a = [c.lower() for c in phrase if c.isalpha()]
    b = [c for c in a[::-1]]
    return a == b


def main():
    print(is_palindrome("A man, a plan, a canal – Panama"))
    print(is_palindrome("Implement a function which works"))
    # print(is_palindrome(123))


if __name__ == '__main__':
    main()
