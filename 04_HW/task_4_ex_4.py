"""
Exercise 4
Implement a function split_by_index(string: str, indexes: List[int]) -> List[str]
which splits the string by indexes specified in indexes.
Only positive index, larger than previous in list is considered valid. Invalid indexes must be ignored.
Examples:
# >>> split_by_index("pythoniscool,isn'tit?", [6, 8, 12, 13, 18])
# ["python", "is", "cool", ",", "isn't", "it?"]
# >>> split_by_index("pythoniscool,isn'tit?", [6, 8, 8, -4, 0, "u", 12, 13, 18])
# ["python", "is", "cool", ",", "isn't", "it?"]
# >>> split_by_index("no luck", [42])
["no luck"]
"""


def split_by_index(phrase, indexes):
    result = []
    start_index = 0
    indexes = [i for i in indexes if type(i) == int and (0 < i < len(phrase))]

    for j, i in enumerate(indexes):
        if j == 0 or i > indexes[j - 1] and i > start_index:
            result.append(phrase[start_index:i])
            start_index = i
    else:
        result.append(phrase[start_index:])
    return result


def main():
    print(split_by_index("pythoniscool,isn'tit?", [6, 8, 5, 6, 12, 13, 18, 40]))
    print(split_by_index("pythoniscool,isn'tit?", [6, 8, 8, -4, 0, "u", 12, 13, 18]))
    print(split_by_index("no luck", [42]))
    print(split_by_index("no luck", [0]))


if __name__ == '__main__':
    main()
