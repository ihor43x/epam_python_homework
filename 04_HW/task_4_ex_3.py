"""
Exercise 3
Implement a function which works the same as str.split.
Note:
- Usage of str.split method is prohibited
- Raise ValueError in case of wrong data type
"""


def custom_split(phrase, splitter=" "):
    if type(phrase) != str:
        raise ValueError

    split_list = []
    prev_splitter = 0
    for i, char in enumerate(phrase):
        if char in splitter and phrase[i-1] not in splitter:
            split_list.append(phrase[prev_splitter:i])
            prev_splitter = i + 1
        elif phrase[i-1] in splitter:
            prev_splitter = i
    else:
        split_list.append(phrase[prev_splitter:])
    return split_list


def main():
    print("A man,  a plan,  a canal  –  Panama".split())
    print(custom_split("A man,  a plan,  a canal  –  Panama", " "))
    print(custom_split("A man, a plan, a canal – Panama", ","))
    print(custom_split("Implement a     function which works"))


if __name__ == '__main__':
    main()
