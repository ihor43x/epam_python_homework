"""
Exercise 1
Implement a function which receives a string and replaces all " symbols
with ' and vise versa.
The function should return modified string.
Usage of any replacing string functions is prohibited.
"""


def convert_quote_mark(phrase):
    result = ''
    for a in phrase:
        result += a if a not in ["'", '"'] else "'" if a == '"' else '"'
    return result

    # Alternative:
    # new_text = ''
    # for s in text:
    #     s = '"' if s == "'" else "'"
    #     if s == "'":
    #         s = '"'
    #     elif s == '"':
    #         s = "'"
    #     new_text += s
    # return new_text


def main():
    print(convert_quote_mark("Some 'Text' to convert"))
    print(convert_quote_mark('Some "Text" to convert'))


if __name__ == '__main__':
    main()
