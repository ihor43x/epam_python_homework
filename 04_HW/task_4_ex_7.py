"""
Exercise 7
Implement a function foo(List[int]) -> List[int] which,
given a list of integers, returns a new or modified
list in which every element at index i of the
new list is the product of all the numbers
in the original array except the one at i.
Example:
foo([1, 2, 3, 4, 5])
[120, 60, 40, 30, 24]
foo([3, 2, 1])
[2, 3, 6]
"""
import math


def foo(num_list):
    result = [math.prod([n for j, n in enumerate(num_list) if j != i]) for i, num in enumerate(num_list)]
    return result

    #  Alternative:
    # result = []
    # for i in range(len(num_list)):
    #     new_list = num_list.copy()
    #     new_list.pop(i)
    #     result.append(math.prod(new_list))
    # return result


def main():
    print(foo([1, 2, 3, 4, 5]))
    print(foo([3, 2, 1]))
    print(foo([3, 0, 1]))
    print(foo([0, 0, 0, 0]))


if __name__ == '__main__':
    main()
