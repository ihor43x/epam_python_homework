"""
Exercise 6
Implement a function get_longest_word(s: str) -> str which returns the longest
word in the given string. The word can contain any symbols except
whitespaces (`,\n,\tand so on). If there are multiple longest words
in the string with a same length return the word that occurs first.
Example:
get_longest_word('Python is simple and effective!’)
#output: 'effective!’
get_longest_word('Any pythonista like namespaces a lot.’)
#output: 'pythonista’
Note:
- Raise ValueError in case of wrong data type
- Usage of 're' library is prohibited
"""


def get_longest_word(phrase):
    if type(phrase) != str:
        raise ValueError
    longest = max([(len(word), word) for word in phrase.split()])
    return longest[1]


def main():
    print(get_longest_word('Python is simple and effective!'))
    print(get_longest_word('Any pythonista like namespaces a lot.'))


if __name__ == '__main__':
    main()
